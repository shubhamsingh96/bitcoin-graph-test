A simple application that displays the price of the BTC-USD channel in realtime using WebHooks.

Graph is used to make it simpler and easy to understand the price changes.

You will see the 10 recent price changes.

current price and the previous price will be displayed as well.