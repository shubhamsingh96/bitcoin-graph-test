import React, {Component} from 'react';
import {SafeAreaView, StyleSheet, View, Text, Dimensions} from 'react-native';
import {LineChart} from 'react-native-chart-kit';

const {width} = Dimensions.get('window');

export default class App extends Component {
  constructor(props) {
    super(props);
    this.ws = '';
  }

  state = {
    priceData: [9000],
  };

  componentDidMount() {
    console.log('ready')
    const subscribe = {
      type: 'subscribe',
      channels: [
        {
          name: 'ticker',
          product_ids: ['BTC-USD'],
        },
      ],
    };
    this.ws = new WebSocket('wss://ws-feed.pro.coinbase.com');
    this.ws.onopen = () => {
      this.ws.send(JSON.stringify(subscribe));
    };
    this.ws.onmessage = (event) => {
      const data = JSON.parse(event.data);
      let getPrice = null;
      if (data && data.price) {
        getPrice = +data.price;
        const copyData = this.state.priceData.concat(getPrice);
        if (copyData.length > 10) {
          copyData.splice(0, 1);
        }
        this.setState({priceData: copyData});
      }
    };
  }

  componentWillUnmount() {
    this.ws.close();
  }

  render() {
    const {priceData} = this.state;
    const chartConfig = {
      backgroundGradientFrom: '#1A237E',
      backgroundGradientFromOpacity: 0.8,
      backgroundGradientTo: '#1A237E',
      backgroundGradientToOpacity: 1,
      color: () => '#fff',
      strokeWidth: 3,
      barPercentage: 5,
      useShadowColorFromDataset: false,
    };
    const previousPrice =
      priceData && priceData.length > 2
        ? priceData[priceData.length - 2]
        : priceData[0];
    const currentPrice = priceData[priceData.length - 1];
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#ECEFF1'}}>
        <View style={{flex: 1, alignItems: 'center'}}>
          <Text style={styles.title}>BitCoin Info </Text>
          <Text style={styles.caption}>BTC-USD</Text>
          <LineChart
            data={{
              // labels: [
              //   '4s',
              //   '8s',
              //   '12s',
              //   '16s',
              //   '20s',
              //   '24s',
              //   '28s',
              //   '32s',
              //   '36s',
              //   '40s',
              // ],
              datasets: [
                {
                  data: priceData,
                },
              ],
            }}
            yAxisInterval={1000}
            width={width - 40}
            height={230}
            verticalLabelRotation={30}
            chartConfig={chartConfig}
            bezier
            style={{
              borderRadius: 15,
            }}
          />
          <View style={styles.infoSection}>
            <View style={styles.infoTabs}>
              <Text style={styles.infoText}>Previous Value : </Text>
              <Text style={styles.infoText}>{previousPrice}</Text>
            </View>
            <View style={styles.infoTabs}>
              <Text style={styles.infoText}>Current Value : </Text>
              <Text style={styles.infoText}>{currentPrice}</Text>
            </View>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 25,
    letterSpacing: 0.7,
    textTransform: 'uppercase',
    marginVertical: 20,
  },
  caption: {
    fontSize: 16,
    marginBottom: 30,
  },
  infoSection: {
    marginTop: 20,
  },
  infoTabs: {
    width: width - 40,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
  },
  infoText: {
    fontSize: 14,
    letterSpacing: 0.4,
    textTransform: 'uppercase',
    fontWeight: '600',
  },
});
